<?php get_header(); ?>

<?php the_post(); ?>  

<div id="page_header" class="section" style="background-image: url(<? the_field('page_header_background'); ?>);">
	<div class="container content_wrapper">
		<h1><?php the_field('page_title'); ?></h1>
	</div>
</div>

<div class="workshop section">
	
	<div class="container content_wrapper">
		<?php the_field('page_content'); ?> 
		
		
		<?php if (!(current_user_can('level_0'))){ ?>
			
			<div class="form-wrapper">
        
            	<!-- <h1 class="text-left"><?php the_title(); ?></h1> -->

				<form action="<?php echo get_option('home'); ?>/wp-login.php" method="post">
					<label>Username</label>
					<input type="text" name="log" id="log" value="<?php echo wp_specialchars(stripslashes($user_login), 1) ?>" size="20" placeholder="" />
					<label>Password</label>
					<input type="password" name="pwd" id="pwd" size="20" />
					<br />
					<input type="submit" name="submit" value="Login" class="a_button" />    
				</form>
									
			</div>
			
			<?php } else { ?>
			
				<div id="the_login">
					
					
				</div>
				
			<?php }  ?>	
		
		
	</div>
	
</div>

 

<?php get_footer(); ?>



