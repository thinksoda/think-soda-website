<?php get_header(); ?>

<?php the_post(); ?>  

<div id="page_header" class="section" style="background-image: url(<? the_field('page_header_background','467'); ?>);">
	<div class="container content_wrapper">
		<h1><?php the_field('page_title','467'); ?></h1>
	</div>
</div>

<div class="section diary_page">
	
	<div class="container content_wrapper">
		
		<?php the_field('page_content','467'); ?> 
	
	
	<ul class="slides diary_loop">
		
		<?php 
			
			$args = array(
				'post_type'=> 'diary',				
				'post_status' => 'publish',
				'orderby'   => 'date',
				'order'    => 'DESC',
				'posts_per_page' => -1								
			);
			
			
			$result = new WP_Query( $args );			
						
			if ( $result-> have_posts() ) : 
			
				while ( $result->have_posts() ) : $result->the_post();
				
					$entry_id = get_the_ID(); 
										
					?>
						<li>
												
							<a class="ts_fizzy_frame" content_id="<? echo $entry_id; ?>" content_type="diary" href="<?php the_permalink(); ?>">
								<div class="hover_veil"></div>
								<img src="<? the_field('thumbnail_image', $entry_id); ?>" alt="" />								
								<h4><? the_field('secondary_title', $entry_id); ?></h4>
							</a>											
						
						</li>
					<?
						
				  				
				endwhile; 
			
			endif; wp_reset_postdata(); 
		?>
		
		</ul>

	
	
	</div>
	
</div>

 

<?php get_footer(); ?>



