
<div id="footer" class="section">
	
	<div class="container">
		
		<ul>			
			<li>&copy; 2004 - <? print(Date("Y")); echo ' '; the_field('the_ts_name', 'options'); ?></li>
			<li>ABN: <? the_field('the_ts_abn', 'options'); ?></li>	
			
			<?php
					
				wp_nav_menu( 
					
				array( 
				
					'theme_location' => 'footer-nav',					
					'container' => '',
					'items_wrap' => '%3$s',
					'container' => false,
				) 
					
			); ?>	
					
		</ul>
		
		<div class="footer_links">
			<p>Proudly supporting</p>

			<a class="ocean" href="https://4ocean.com/" title="Think Soda supports 4 Ocean!" rel="noopener" target="_blank">				
				<img src="/wp-content/themes/TS/images/4_ocean.png" alt="" />
			</a>

			<a class="upparel" href="https://upparel.com.au/" title="Think Soda supports Upparel!" rel="noopener" target="_blank">				
				<img src="/wp-content/themes/TS/images/upparel-logo.svg" alt="" />
			</a>

			
		</div>
		
		
	</div>
	
</div>


<!-- MODAL & POP UP CONTENT -->
<div id="modal_veil"></div>

<div id="close_modal_wrapper"><a id="close_modal" title="Click here to close this modal window"><i class="far fa-times"></i></a></div>

<div id="window_wrapper">
	
	<div id="window_content">
		
		
	</div>
	
</div>

<!-- PRELOAD IMAGES (old skooooooooool!) -->
<img src="/wp-content/themes/TS/images/blinking.gif" alt="" class="pre_loading" />

<!-- GOOGLE ANALYTICS -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-159381403-1"></script>

<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());
	gtag('config', 'UA-159381403-1');
</script>

<!-- CUSTOM JS -->
<script type="text/javascript" src="/wp-content/themes/TS/js/custom_scripts.js?v=6"></script>

<?php wp_footer(); ?>

</body>

</html>