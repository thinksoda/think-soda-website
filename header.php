<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">

<title><?php wp_title(''); ?></title>

<meta name="description" content="A collection of projects through time and space handled independently or as part of a team.">
	
<!-- JQUERY -->
<script type="text/javascript" src="/wp-content/themes/TS/js/jquery-3.6.0.min.js"></script>

<!-- CSS JS FAV -->
<link rel="stylesheet" type="text/css" href="/wp-content/themes/TS/style.css?v=2142" />
<link rel="shortcut icon" href="/wp-content/themes/TS/favicon.png" />

<!-- DEVICE ORIENTED -->
<meta name="viewport" content="initial-scale=0.82">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />

<?php wp_head(); ?>

<?php
	
	$page_id = get_the_ID(); 
	
	if($page_id !== 248){
		$logo_style = 'display: inline-block';		
	}
	
?>

 
</head> 

<? flush(); ?>

<body>	
	
	<!-- SPACE -->
	<div id="header_space"></div>	
	
	<!-- HEADER -->
	
	<div id="header" class="section">
		
		<div class="container">
		
			<ul id="primary_nav">
				
				<li id="menu_logo"><a href="/" title="Click here to go Home"><img id="the_logo" style="<? echo $logo_style; ?>;" src="<?php the_field('secondary_ts_brandmark','options'); ?>" alt="" /></a></li>				
				
				<li id="start_here"><a href="/home#the_introduction" title="Click here to Start!">Start here!</a></li>
				
				<li><a href="/portfolio" title="Click here to go to the Portfolio Archive">Portfolio</a></li>
				
				<li id="start_diary"><a href="/diary" title="Click here to visit to the TS Diary">Diary</a></li>
				
				<?php
					
					wp_nav_menu( 
						
					array( 
					
						'theme_location' => 'primary-nav',
						'menu_id' => 'primary_nav',
						'menu_class' => '',
						'container' => '',
						'items_wrap' => '%3$s',
						'container' => false,
					) 
						
				); ?>	
			
			</ul>
		
		</div>
		
	</div>

		
	