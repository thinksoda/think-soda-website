<?php get_header(); ?>

<?php the_post(); ?>  

<div id="page_header" class="section" style="background-image: url(<? the_field('page_header_background'); ?>);">
	<div class="container content_wrapper">
		<h1><?php the_field('page_title'); ?></h1>
	</div>
</div>

<div id="portfolio_page" class="section">
	
	<div class="container content_wrapper">
		
		<div class="filters">
			
			<h3>Field</h3>
			
			<ul id="field" class="fields_system_skills">
			
				<?php   
					
					$terms = get_terms(
						
					    array(
					        'taxonomy'   => 'project_taxonomy_field',
					        'hide_empty' => true,
					    )
					);
		
				    foreach ( $terms as $term ) { 				    
				    	?><li active="no" keyword="<?php echo $term->slug; ?>"><?php echo $term->name; ?></li><?php
				    }
						
				?>
	
			</ul>
	
			<h3>CMS / System</h3>
			
			<ul id="cms" class="fields_system_skills">
			
				<?php   
					
					$terms = get_terms(
						
					    array(
					        'taxonomy'   => 'project_taxonomy_cms',
					        'hide_empty' => true,
					    )
					);
		
				    foreach ( $terms as $term ) { 				    
				    	?><li active="no" keyword="<?php echo $term->slug; ?>"><?php echo $term->name; ?></li><?php
				    }
						
				?>
	
			</ul>
			
			<h3>Skills / Tools</h3>
			
			<ul id="skill" class="fields_system_skills">
			
				<?php   
					
					$terms = get_terms(
						
					    array(
					        'taxonomy'   => 'project_taxonomy_skills',
					        'hide_empty' => true,
					    )
					);
		
				    foreach ( $terms as $term ) { 				    
				    	?><li active="no" keyword="<?php echo $term->slug; ?>"><?php echo $term->name; ?></li><?php
				    }
						
				?>
	
			</ul>
			
			<h3>Language</h3>
			
			<ul id="language" class="fields_system_skills">
			
				<?php   
					
					$terms = get_terms(
						
					    array(
					        'taxonomy'   => 'project_taxonomy_language',
					        'hide_empty' => true,
					    )
					);
		
				    foreach ( $terms as $term ) { 				    
				    	?><li active="no" keyword="<?php echo $term->slug; ?>"><?php echo $term->name; ?></li><?php
				    }
						
				?>
		
			</ul>
			
			<div id="reset" class="a_button">reset</div>
			
		</div>
			
		<ul class="slides portfolio_loop">
		
		<?php 
			
			$args = array(
				'post_type'=> 'project',				
				'post_status' => 'publish',
				'orderby'   => 'menu_order',
				'order'    => 'ASC',				
				'posts_per_page' => -1 
								
			);
			
			
			$result = new WP_Query( $args );			
						
			if ( $result-> have_posts() ) : 
			
				while ( $result->have_posts() ) : $result->the_post();
				
					$project_id = get_the_ID();
				
					// PARTNER
					
					$ts_project = get_field('think_soda_project',$project_id);
					if($ts_project=='yes'){
						
						$partner_image_url = get_field('ts_project_image','options');
						$partner_name = '';
						
					}else{
						
						$post_object = get_field('partner_contract_name');

						if( $post_object ): 
						
							$post = $post_object;
							setup_postdata( $post ); 
							
								$partner_image_url = get_field('partner_logo');
								$partner_name = ' with '.get_field('partner_name');					
							
							wp_reset_postdata();
							
						endif;

					}
					
					// FIELD							
					
					$fields = array();
					$terms = get_field('field', $project_id);
					
					if( $terms ): 
					     foreach( $terms as $term ): 
					     
					     	$item = $term->slug;
						 	array_push($fields, $item);
						 	
						 endforeach; 
						 
					endif; 			
					
					// CMS							
					
					$cms_systems = array();
					$terms = get_field('cms_system', $project_id);
					
					if( $terms ): 
					     foreach( $terms as $term ): 
					     
					     	$item = $term->slug;
						 	array_push($cms_systems, $item);
						 	
						 endforeach; 
						 
					endif; 			
					
					// SKILLS							
					
					$skills = array();
					$terms = get_field('skill', $project_id);
					
					if( $terms ): 
					     foreach( $terms as $term ): 
					     
					     	$item = $term->slug;
						 	array_push($skills, $item);
						 	
						 endforeach; 
						 
					endif; 			
					
					// LANGUAGE					
					
					$languages = array();
					$terms = get_field('language', $project_id);
					
					if( $terms ): 
					     foreach( $terms as $term ): 
					     
					     	$item = $term->slug;
						 	array_push($languages, $item);
						 	
						 endforeach; 
						 
					endif; 				
					
					?>
						<li keyword="<? echo implode(" ", $cms_systems); ?> <? echo implode(" ", $fields); ?> <? echo implode(" ", $skills); ?> <? echo implode(" ", $languages); ?>">						
							
							<a href="" class="portfolio-tile" title="<? echo get_the_title($project_id); echo $partner_name; ?>">
								<div class="hover_veil"></div>
								<div class="quick_intro"><p><? the_field('quick_description', $project_id); ?></p></div>
								<img src="<? the_field('thumbnail', $project_id); ?>" alt="" />
								<img class="partner_logo" src="<? echo $partner_image_url; ?>" alt="" />
							</a>											
							
						</li>
					<?
					
						
				  				
				endwhile; 
			
			endif; wp_reset_postdata(); 
		?>
		
		</ul>

				
	</div>
	
</div>

<script>
	
	$(document).ready(function(){
		
		// QUERY STRING	
		setTimeout(function(){
			
			const the_url = new URLSearchParams(window.location.search);
			const the_query_string = the_url.get('view');
			$('ul#field li[keyword="'+the_query_string+'"]').trigger('click');
	
		}, 100);
		
		
		$('ul.fields_system_skills li').each(function(){
			
			$(this).click(function(){
				
				var is_active = $(this).attr('active');
				var what_is_clicked = $(this).attr('keyword');
				var what_category = $(this).parent().attr('id');
				
				if(is_active=='no'){
					$('ul.fields_system_skills li').attr('active', 'no');
					$('ul.fields_system_skills li').removeClass('active');
					$(this).addClass('active');
					$(this).attr('active','yes');
					process_items_to_view();	
				}
				
				if(is_active=='yes'){
					$('ul.fields_system_skills li').removeClass('active');
					$('ul.fields_system_skills li').attr('active','no');			
					$('ul.portfolio_loop li').show();					
				}				
							
				handle_footer();
			});
			
		});
		
		
		$('#reset').click(function(){
			
			$('ul.fields_system_skills li').removeClass('active');
			$('ul.fields_system_skills li').attr('active','no');			
			$('ul.portfolio_loop li').show();
			handle_footer();
			
		});
						
		function process_items_to_view(){
			
			var keywords = [];
			
			$('ul.fields_system_skills li').each(function(){
						
				var is_active = $(this).attr('active');
						
				if(is_active=='yes'){
					
					var the_keyword = $(this).attr('keyword');
					keywords.push(the_keyword); 
				}
						
			});
			
			$('ul.portfolio_loop li').hide();
					
			$('ul.portfolio_loop li').each(function(){
				
				
				var this_ones_keywords = [];
				var this_attr = $(this).attr('keyword');
				var this_ones_keywords = this_attr.split(" ");		
							
				function arrayContainsArray (superset, subset) {
				  if (0 === subset.length) {
				    return false;
				  }
				  return subset.every(function (value) {
				    return (superset.indexOf(value) >= 0);
				  });
				}
				
				if( arrayContainsArray (this_ones_keywords, keywords) > 0)
					$(this).show();
				
				
			});
		
		}
		
	});
	
</script>

<?php get_footer(); ?>