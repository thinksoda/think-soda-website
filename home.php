<?php
/*
Template Name: Home
*/
?>

<?php get_header(); ?>


<!-- HOME SLIDER -->

<div id="home_slider_wrapper" class="section">
	
	<div id="master_brandmark" style="background-image: url(<?php the_field('master_ts_brandmark','options'); ?>);"></div>
	
	
	<div id="watermark" style="background-image: url(<?php the_field('slide_watermark','options'); ?>);"></div>
			
	<ul id="home_slider" class="slides">
		
		<?php 
			
			$args = array( 
				'posts_per_page' => 100, 
				'post_type' => array( 'slide'),
				'orderby'   => 'menu_order',
				'order'    => 'ASC',
			);
						
			$result = new WP_Query( $args );
			
			if ( $result-> have_posts() ) : 
			while ( $result->have_posts() ) : $result->the_post();
			
				?>
					<li style="background-image: url(<?php the_field('slide_image'); ?>);">
						<img src="<?php the_field('slide_image'); ?>" alt="<?php the_field('photo_title'); ?>, by <?php the_field('photo_credit'); ?>" />							
					</li>
				
				<?
			  
			
			endwhile; 
			
			endif; wp_reset_postdata(); 
		?>

		
		</ul>
		
</div>

<div id="the_introduction" class="section content_wrapper">
	
	<div class="container">
		<h1><? the_field('main_title','options'); ?></h1>
		<h2><? the_field('secondary_intro','options'); ?></h2>
	
	</div>
	
</div>

<div id="" class="section content_wrapper grey">
	
	<div class="container flexslider carousel" >
		
		<h3>Think Soda Projects</h3>
		
		<ul class="slides feed_loop">
		
		<?php 
			
			$args = array(
				'post_type'=> 'project',
				'orderby'    => 'ID',				
				'post_status' => 'publish',
				'orderby'   => 'menu_order',
				'order'    => 'ASC',
				
				'meta_query' => array(
					 array(
					 	'key' => 'think_soda_project',
					 	'value' => 'yes',
					 ),
					 array(
					 	'key'     => 'front_page_project',
					 	'value'   => 'yes',
					 ),
				 
				),
				
				'posts_per_page' => 16 
								
			);
			
			$the_count = 0;
			$result = new WP_Query( $args );						
						
			if ( $result-> have_posts() ) : 
			
				while ( $result->have_posts() ) : $result->the_post();
				
					$project_id = get_the_ID();
					
					if($the_count==0){
						echo '<li>';	
					}
					
					?>						
						<a href="" class="portfolio-tile" title="<? echo get_the_title($project_id); ?>">
							<div class="hover_veil"></div>
							<div class="quick_intro"><p><? the_field('quick_description', $project_id); ?></p></div>
							<img src="<? the_field('thumbnail', $project_id); ?>" alt="" />								
						</a>										
					<?
												
					$the_count ++;
					
					if($the_count==8){
						echo '</li>';	
						$the_count = 0;
					}
				  				
				endwhile; 
			
			endif; wp_reset_postdata(); 
		?>
		
		</ul>
		
	</div>
	
</div>

<div id="" class="section content_wrapper">
	
	<div class="container flexslider carousel" >
		
		<h3>Partner / Contract Projects</h3>
		
		<ul class="slides feed_loop">
		
		<?php 
			
			$args = array(
				'post_type'=> 'project',				
				'orderby'    => 'ID',
				'post_status' => 'publish',
				'orderby'   => 'menu_order',
				'order'    => 'ASC',
				'meta_query' => array(
					 array(
					 	'key' => 'think_soda_project',
					 	'value' => 'no',
					 ),
					 array(
					 	'key'     => 'front_page_project',
					 	'value'   => 'yes',
					 ),
				 
				),
				'posts_per_page' => 24 
								
			);
			
			$the_count = 0;
			$result = new WP_Query( $args );			
						
			if ( $result-> have_posts() ) : 
			
				while ( $result->have_posts() ) : $result->the_post();
				
					$project_id = get_the_ID();
				
					// PARTNER
					
					$post_object = get_field('partner_contract_name');

					if( $post_object ): 
					
						$post = $post_object;
						setup_postdata( $post ); 
						
							$partner_image_url = get_field('partner_logo');
							$partner_name = get_field('partner_name');					
						
						wp_reset_postdata();
						
					endif;
					
					if($the_count==0){
						echo '<li>';	
					}
					
					?>						
						<a href="" class="portfolio-tile" title="<? echo get_the_title($project_id); ?> with <? echo $partner_name; ?>">
							<div class="hover_veil"></div>
							<div class="quick_intro"><p><? the_field('quick_description', $project_id); ?></p></div>
							<img src="<? the_field('thumbnail', $project_id); ?>" alt="" />
							<img class="partner_logo" src="<? echo $partner_image_url; ?>" alt="" />
						</a>											
					<?
						
					$the_count ++;
					
					if($the_count==8){
						echo '</li>';	
						$the_count = 0;
					}
					
						
				  				
				endwhile; 
			
			endif; wp_reset_postdata(); 
		?>
		
		</ul>
		
		
		<!-- <h3>See all Projects by category / skill </h3> -->
				
	</div>
	
</div>

<!-- SEE EVERYTHING -->

<div id="see_everything" class="section content_wrapper parallax-window" data-parallax="scroll" data-image-src="<? the_field('parallax','options'); ?>">

	<div class="container">
		
		<h3><a href="/portfolio">All Projects</a></h3>
		
		<ul>
			
			<li><a href="/portfolio?view=design"> by Design</a></li>
			<li><a href="/portfolio?view=ux"> by UX</a></li>
			<li><a href="/portfolio?view=front-end"> by Development</a></li>
			<li><a href="/portfolio?view=hosting"> by Hosting</a></li>
						
		</ul>
		
	</div>
	
</div>

<!-- THE DIARY -->

<div id="the_diary" class="section content_wrapper grey">
	
	<div class="container">
		
		<h3>Diary</h3>
		
		<ul class="slides diary_loop">
		
		<?php 
			
			$args = array(
				'post_type'=> 'diary',				
				'post_status' => 'publish',
				'orderby'   => 'date',
				'order'    => 'DESC',
				'meta_key' => 'on_front_page',
				'meta_value' => 'yes',
				'posts_per_page' => 12								
			);
			
			
			$result = new WP_Query( $args );			
						
			if ( $result-> have_posts() ) : 
			
				while ( $result->have_posts() ) : $result->the_post();
				
					$entry_id = get_the_ID(); 
										
					?>
						<li>
												
							<a class="ts_fizzy_frame" content_id="<? echo $entry_id; ?>" content_type="diary" href="<?php the_permalink(); ?>">
								<div class="hover_veil"></div>
								<img src="<? the_field('thumbnail_image', $entry_id); ?>" alt="" />								
								<h4><? the_field('secondary_title', $entry_id); ?></h4>
							</a>											
						
						</li>
					<?
						
				  				
				endwhile; 
			
			endif; wp_reset_postdata(); 
		?>
		
		</ul>
		
		<a href="/diary" class="read_more" title="Read more from the TS Diary">More from the Diary <i class="fas fa-arrow-right"></i></a>
		
	</div>
	
</div>

<!-- HAN SOLO -->

<div id="han_solo" class="section content_wrapper parallax-window" data-parallax="scroll" data-image-src="<? the_field('han_solo_image','options'); ?>">
	
	<div class="container">
		
		<h4><? the_field('han_solo_quote','options'); ?></h4>
		<p><i><? the_field('han_solo_description','options'); ?></i></p>
	</div>
	
</div>

<!-- THE SKILLS HILL -->

<div id="the_skills_hill" class="section">
	
	<div class="container">
	
		<ul>
		
			<?php
		
				if( have_rows('the_skills_hill', 'options') ):
				    
				    while ( have_rows('the_skills_hill', 'options') ) : the_row();
				       
				        ?>			        	
				        	<li>
				        		<a href="https://<? the_sub_field('link'); ?>" target="_blank" rel="noopener" title="<? the_sub_field('alt_text'); ?>"><img src="<? the_sub_field('logo'); ?>" alt="" /></a>
				        	</li>	
						
						<?
							
				    endwhile;
				
				else :
				
				endif;
				
			?>
		
		</ul>
	
	</div>
	
</div>

<script>

	$(document).ready(function(){
		
		show_logo(); 
		
		//	hide_home_url();
				
		// INITIATE SLIDERS
		$('#home_slider_wrapper').flexslider({
		 	animation: "fade",
		 	directionNav: false, 	 	
		 	animationLoop: true,
		 	controlNav: "",		 	
		 	slideshow: true,
		 	slideshowSpeed: 4000,
		});
						
		$('.flexslider').flexslider({
			animation: "slide",
			slideshow: false,
			animationLoop: true,
			directionNav: false,			
		}); 

		// START HERE
		$('#start_here').click(function(event){
			
			event.preventDefault();
				
			$([document.documentElement, document.body]).animate({
		 		scrollTop: $("#the_introduction").offset().top - 37
    		}, 500);

		});	
		
		// START DIARY
		$('#start_diary').click(function(event){
					
			event.preventDefault();

			$([document.documentElement, document.body]).animate({
		 		scrollTop: $("#the_diary").offset().top - 37
    		}, 500);

		});	
		
		
			
		
  	});
		
</script>

<?php get_footer(); ?>