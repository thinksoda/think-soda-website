<?php get_header(); ?>

<?php the_post(); ?>  

<div id="page_header" class="section" style="background-image: url(<? the_field('page_header_background'); ?>);">
	<div class="container content_wrapper">
		<h1><?php the_field('page_title'); ?></h1>
	</div>
</div>

<div class="section">
	
	<div class="container content_wrapper">
		<?php the_field('page_content'); ?> 
	</div>
	
</div>

 

<?php get_footer(); ?>



