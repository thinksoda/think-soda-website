<?php
//
// THEME FUNCTIONS - Business Hub - Nick Kind, Immediate 190516
//
//
// FUNCTIONS BEGIN 
//
//
//
// REMOVE THINGS WE DONT NEED
//
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

    add_action('init', 'tjnz_head_cleanup');
    function tjnz_head_cleanup() {
        remove_action( 'wp_head', 'feed_links_extra', 3 );                      // Category Feeds
        remove_action( 'wp_head', 'feed_links', 2 );                            // Post and Comment Feeds
        remove_action( 'wp_head', 'rsd_link' );                                 // EditURI link
        remove_action( 'wp_head', 'wlwmanifest_link' );                         // Windows Live Writer
        remove_action( 'wp_head', 'index_rel_link' );                           // index link
        remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );              // previous link
        remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );               // start link
        remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );   // Links for Adjacent Posts
        remove_action( 'wp_head', 'wp_generator' );                             // WP version
        if (!is_admin()) {
            wp_deregister_script('jquery');                                     // De-Register jQuery
            wp_register_script('jquery', '', '', '', true);                     // Register as 'empty', because we manually insert our script in header.php
        }
    }

// remove WP version from RSS
add_filter('the_generator', 'tjnz_rss_version');
function tjnz_rss_version() { return ''; }
//
// REMOVE GUTENBERG BLOCK LIBRARY CSS FROM LOADING ON THE FRONTEND
//
function smartwp_remove_wp_block_library_css(){
    wp_dequeue_style( 'wp-block-library' );
    wp_dequeue_style( 'wp-block-library-theme' );
    wp_dequeue_style( 'wc-block-style' ); // Remove WooCommerce block CSS
} 
add_action( 'wp_enqueue_scripts', 'smartwp_remove_wp_block_library_css', 100 );
//
// REGISTER MENUS
//
//
function register_my_menu() {
	register_nav_menu('top-nav',__( 'Top Nav' ));
	register_nav_menu('primary-nav',__( 'Primary Nav' ));
	register_nav_menu('footer-nav',__( 'Footer Nav' ));
}
add_action( 'init', 'register_my_menu' );
//
// SUPPORT THUMBNAILS
//
add_theme_support( 'post-thumbnails' );
//
//
// THUMBNAIL SIZES
//
add_image_size( 'blog-thumb', 375, 200, false );
add_image_size( 'standard-image', 840, 490, false );
add_filter( 'image_size_names_choose', 'think_soda_custom_thumbnails' );

function think_soda_custom_thumbnails( $sizes ) {
    return array_merge( $sizes, array(
        'blog-thumb' => __('Blog Thumb'),
        'standard-image' => __('Standard Image'),
    ) );
}
//
//
// ALLOW SVG
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');
//
//
//
// CUSTOM LOGIN PAGE
//
function na_login_header() {
	get_header();
}
add_action( 'login_header', 'na_login_header' );
//
function na_login_footer() {
	get_footer();
}
add_action( 'login_footer', 'na_login_footer' );
//
// FROM EMAIL
//
function res_fromemail($email) {
    $wpfrom = get_option('admin_email');
    return $wpfrom;
}
function res_fromname($email){
    $wpfrom = get_option('blogname');
    return $wpfrom;
}
add_filter('wp_mail_from', 'res_fromemail');
add_filter('wp_mail_from_name', 'res_fromname');
//
// ACF OPTIONS PAGE
//
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'General',
		'menu_title'	=> 'General',
		'menu_slug' 	=> 'web-info',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));	
}
// 
// SLIDES
//
add_action( 'init', 'my_custom_post_slide' );
function my_custom_post_slide() {
	$labels = array(
		'name'               => _x( 'Slides', 'post type general name' ),
		'singular_name'      => _x( 'Slide', 'post type singular name' ),
		'add_new'            => _x( 'Add New', 'Slides' ),
		'add_new_item'       => __( 'Add New Slide' ),
		'edit_item'          => __( 'Edit Slide' ),
		'new_item'           => __( 'New Slide' ),
		'all_items'          => __( 'All Slides' ),
		'view_item'          => __( 'View Slides' ),
		'search_items'       => __( 'Search Slides' ),
		'not_found'          => __( 'No Slides found' ),
		'not_found_in_trash' => __( 'No Slides found in the Trash' ),
		'parent_item_colon'  => '',
		'menu_name'          => 'Slides'
	);
	$args = array(
		'labels'        => $labels,
		'public'        => true,
		'hierarchical'	=> true,
		'show_in_nav_menus' => false,
		'supports'      => array( 'title', 'author'),
		'has_archive'   => true
	);
	register_post_type( 'slide', $args );
}
add_action( 'init', 'my_custom_post_slide' );
// 
// PROJECTS
//
add_action( 'init', 'my_custom_post_project' );
function my_custom_post_project() {
	$labels = array(
		'name'               => _x( 'Projects', 'post type general name' ),
		'singular_name'      => _x( 'Project', 'post type singular name' ),
		'add_new'            => _x( 'Add New', 'Project' ),
		'add_new_item'       => __( 'Add New Project' ),
		'edit_item'          => __( 'Edit Project' ),
		'new_item'           => __( 'New Project' ),
		'all_items'          => __( 'All Projects' ),
		'view_item'          => __( 'View Projects' ),
		'search_items'       => __( 'Search Projects' ),
		'not_found'          => __( 'No Projects found' ),
		'not_found_in_trash' => __( 'No Projects found in the Trash' ),
		'parent_item_colon'  => '',
		'menu_name'          => 'Projects'
	);
	$args = array(
		'labels'        => $labels,
		'public'        => true,
		'hierarchical'	=> true,
		'show_in_nav_menus' => false,
		'supports'      => array( 'title', 'author'),
		'has_archive'   => true
	);
	register_post_type( 'project', $args );
}
add_action( 'init', 'my_custom_post_project' );
//
// PROJECT TAXONOMY (skills tools etc)
//
add_action('init', 'project_taxonomy_skills');

function project_taxonomy_skills() {
$labels = array(
    'name'                          => 'Skills',
    'singular_name'                 => 'Skill',
    'search_items'                  => 'Search Skills',
    'popular_items'                 => 'Popular Skills',
    'all_items'                     => 'All Skills',
    'parent_item'                   => 'Parent Skill',
    'edit_item'                     => 'Edit Skill',
    'update_item'                   => 'Update Skills',
    'add_new_item'                  => 'Add New Skill',
    'new_item_name'                 => 'New Skill',
    'separate_items_with_commas'    => 'Separate Skills with commas',
    'add_or_remove_items'           => 'Add or remove Skill',
    'choose_from_most_used'         => 'Choose from most used Skills'
    );

$args = array(
    'label'                         => '',
    'labels'                        => $labels,
    'public'                        => false,
    'hierarchical'                  => false,
    'show_ui'                       => false,
    'show_in_nav_menus'             => false,
    'args'                          => array( 'orderby' => 'term_order' ),    
    'query_var'                     => true
);

register_taxonomy( 'project_taxonomy_skills', array('project'), $args );
}
//
// PROJECT TAXONOMY (field - design, front end etc)
//
add_action('init', 'project_taxonomy_field');

function project_taxonomy_field() {
$labels = array(
    'name'                          => 'Field',
    'singular_name'                 => 'Field',
    'search_items'                  => 'Search Field',
    'popular_items'                 => 'Popular Field',
    'all_items'                     => 'All Field',
    'parent_item'                   => 'Parent Field',
    'edit_item'                     => 'Edit Field',
    'update_item'                   => 'Update Field',
    'add_new_item'                  => 'Add New Field',
    'new_item_name'                 => 'New Field',
    'separate_items_with_commas'    => 'Separate Field with commas',
    'add_or_remove_items'           => 'Add or remove Field',
    'choose_from_most_used'         => 'Choose from most used Field'
    );

$args = array(
    'label'                         => '',
    'labels'                        => $labels,
    'public'                        => false,
    'hierarchical'                  => false,
    'show_ui'                       => false,
    'show_in_nav_menus'             => false,
    'args'                          => array( 'orderby' => 'term_order' ),    
    'query_var'                     => true
);

register_taxonomy( 'project_taxonomy_field', array('project'), $args );
}
//
// PROJECT TAXONOMY (cms - wordpress or ther)
//
add_action('init', 'project_taxonomy_cms');

function project_taxonomy_cms() {
$labels = array(
    'name'                          => 'CMS',
    'singular_name'                 => 'CMS',
    'search_items'                  => 'Search CMS',
    'popular_items'                 => 'Popular CMS',
    'all_items'                     => 'All CMS',
    'parent_item'                   => 'Parent CMS',
    'edit_item'                     => 'Edit CMS',
    'update_item'                   => 'Update CMS',
    'add_new_item'                  => 'Add New CMS',
    'new_item_name'                 => 'New CMS',
    'separate_items_with_commas'    => 'Separate CMS with commas',
    'add_or_remove_items'           => 'Add or remove CMS',
    'choose_from_most_used'         => 'Choose from most used CMS'
    );

$args = array(
    'label'                         => '',
    'labels'                        => $labels,
    'public'                        => false,
    'hierarchical'                  => false,
    'show_ui'                       => false,
    'show_in_nav_menus'             => false,
    'args'                          => array( 'orderby' => 'term_order' ),    
    'query_var'                     => true
);

register_taxonomy( 'project_taxonomy_cms', array('project'), $args );
}
//
// PROJECT TAXONOMY (language)
//
add_action('init', 'project_taxonomy_language');

function project_taxonomy_language() {
$labels = array(
    'name'                          => 'Language',
    'singular_name'                 => 'Language',
    'search_items'                  => 'Search Language',
    'popular_items'                 => 'Popular Language',
    'all_items'                     => 'All Language',
    'parent_item'                   => 'Parent Language',
    'edit_item'                     => 'Edit Language',
    'update_item'                   => 'Update Language',
    'add_new_item'                  => 'Add New Language',
    'new_item_name'                 => 'New Language',
    'separate_items_with_commas'    => 'Separate Language with commas',
    'add_or_remove_items'           => 'Add or remove Language',
    'choose_from_most_used'         => 'Choose from most used Language'
    );

$args = array(
    'label'                         => '',
    'labels'                        => $labels,
    'public'                        => false,
    'hierarchical'                  => false,
    'show_ui'                       => false,
    'show_in_nav_menus'             => false,
    'args'                          => array( 'orderby' => 'term_order' ),    
    'query_var'                     => true
);

register_taxonomy( 'project_taxonomy_language', array('project'), $args );
}
// 
// PARTNERS
//
add_action( 'init', 'my_custom_post_partner');
function my_custom_post_partner() {
	$labels = array(
		'name'               => _x( 'Partner', 'post type general name' ),
		'singular_name'      => _x( 'Partner', 'post type singular name' ),
		'add_new'            => _x( 'Add New', 'Partner' ),
		'add_new_item'       => __( 'Add New Partner' ),
		'edit_item'          => __( 'Edit Partner' ),
		'new_item'           => __( 'New Partner' ),
		'all_items'          => __( 'All Partners' ),
		'view_item'          => __( 'View Partners' ),
		'search_items'       => __( 'Search Partners' ),
		'not_found'          => __( 'No Partners found' ),
		'not_found_in_trash' => __( 'No Partners found in the Trash' ),
		'parent_item_colon'  => '',
		'menu_name'          => 'Partners'
	);
	$args = array(
		'labels'        => $labels,
		'public'        => true,
		'hierarchical'	=> true,
		'show_in_nav_menus' => false,
		'supports'      => array( 'title', 'author'),
		'has_archive'   => true
	);
	register_post_type( 'partner', $args );
}
add_action( 'init', 'my_custom_post_partner' );
// 
// DIARY
//
add_action( 'init', 'my_custom_post_diary');
function my_custom_post_diary() {
	$labels = array(
		'name'               => _x( 'Entry', 'post type general name' ),
		'singular_name'      => _x( 'Entry', 'post type singular name' ),
		'add_new'            => _x( 'Add New', 'Entry' ),
		'add_new_item'       => __( 'Add New Entry' ),
		'edit_item'          => __( 'Edit Entry' ),
		'new_item'           => __( 'New Entry' ),
		'all_items'          => __( 'All Entries' ),
		'view_item'          => __( 'View Entries' ),
		'search_items'       => __( 'Search Entries' ),
		'not_found'          => __( 'No Entries found' ),
		'not_found_in_trash' => __( 'No Entries found in the Trash' ),
		'parent_item_colon'  => '',
		'menu_name'          => 'Diary'
	);
	$args = array(
		'labels'        => $labels,
		'public'        => true,
		'hierarchical'	=> true,
		'show_in_nav_menus' => false,
		'supports'      => array( 'title', 'author'),
		'has_archive'   => true
	);
	register_post_type( 'diary', $args );
}
add_action( 'init', 'my_custom_post_diary' );
//
// DIARY TAXONOMY (skills tools learning etc)
//
add_action('init', 'diary_taxonomy');

function diary_taxonomy() {
$labels = array(
    'name'                          => 'Category',
    'singular_name'                 => 'Category',
    'search_items'                  => 'Search Categories',
    'popular_items'                 => 'Popular Categories',
    'all_items'                     => 'All Skills',
    'parent_item'                   => 'Parent Category',
    'edit_item'                     => 'Edit Category',
    'update_item'                   => 'Update Categories',
    'add_new_item'                  => 'Add New Category',
    'new_item_name'                 => 'New Category',
    'separate_items_with_commas'    => 'Separate Categories with commas',
    'add_or_remove_items'           => 'Add or remove Category',
    'choose_from_most_used'         => 'Choose from most used Categories'
    );

$args = array(
    'label'                         => '',
    'labels'                        => $labels,
    'public'                        => false,
    'hierarchical'                  => false,
    'show_ui'                       => false,
    'show_in_nav_menus'             => false,
    'args'                          => array( 'orderby' => 'term_order' ),    
    'query_var'                     => true
);

register_taxonomy( 'diary_taxonomy', array('diary'), $args );
}
//
//
//
// ACF LOCAL JSON 
//
function site_acf_json_save_point($path) {
    $path = get_stylesheet_directory() . '/acf/';
    return $path;
}
add_filter('acf/settings/save_json','site_acf_json_save_point');

function site_json_load_point( $paths ) {

    // remove original path (optional)
    unset($paths[0]);

    // append path
    $paths[] = get_stylesheet_directory() . '/acf/';

    // return
    return $paths;

}
add_filter('acf/settings/load_json', 'site_json_load_point');
//
//
//
// ADD AJAX TO WORDPRESS HEADER
//
add_action( 'wp_head', 'my_ajaxurl' );
function my_ajaxurl() {
	$html = '<script type="text/javascript">';
	$html .= 'var ajaxurl = "' . admin_url( 'admin-ajax.php' ) . '"';
	$html .= '</script>';
	echo $html;
}
//
// MODAL CONTENT 
//
add_action( 'wp_ajax_modal_content', 'modal_content' );
add_action( 'wp_ajax_nopriv_modal_content', 'modal_content' );

function modal_content() {
	
	// GET VARS
	$content_type = $_POST['send_content_type'];	
	$content_id = $_POST['send_content_id'];
	
	// FOR CONTENT TYPE
	
	if($content_type=='diary'){
	
		// OUTPUT CONTENT
		
		?>
		
			<div class="section the_diary_wrapper">
	
				<div class="container">
			
					<div id="diary_wrapper">
						
						<img src="<? the_field('hero_image', $content_id); ?>" alt="" />
						
						<div class="the_padding">	
							
							<?php if( get_field('summary', $content_id) ): ?>
						    	<h1><?php the_field('summary', $content_id); ?></h1>
							<?php endif; ?>
							
							<?php if( get_field('content', $content_id) ): ?>
						    	<?php the_field('content', $content_id); ?>
							<?php endif; ?>
							
						</div>
						
					</div>
					
				</div>
				
			</div>

		
		<?
	}
		
			
	wp_die();
}
//
//
// DISABLE GRVITY FORMS ANCHOR
add_filter( 'gform_confirmation_anchor_1', '__return_false' );
//
//
//
//
// END FUNCTIONS
//
//
//
?>