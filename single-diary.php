<?php get_header(); ?>

<?php the_post(); ?>  

<div class="section the_diary_wrapper">
	
	<div class="container">

		<div id="diary_wrapper">
			
			<img src="<? the_field('hero_image'); ?>" alt="" />
			
			<div class="the_padding">	
				
				<?php if( get_field('summary') ): ?>
			    	<h1><?php the_field('summary'); ?></h1>
				<?php endif; ?>
				
				<?php if( get_field('content') ): ?>
			    	<?php the_field('content'); ?>
				<?php endif; ?>
				
			</div>
			
		</div>
		
	</div>
	
</div>

<?php get_footer(); ?>



